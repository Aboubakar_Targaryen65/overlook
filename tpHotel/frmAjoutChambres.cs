﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class frmAjoutChambres : Form
    {
        public frmAjoutChambres()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels())
            {
                if (item.Nom == cbHôtel.Text)
                {
                    Persistance.ajouteChambres((int)numEtage.Value, txtDescription.Text, item.Id);
                }
            }


        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            numEtage.Value = 0;
            txtDescription.Text = "";
            cbHôtel.Text = "";
        }

        private void cbHôtel_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void frmAjoutChambres_Load(object sender, EventArgs e)
        {
           

            foreach (Hotel item in Persistance.getLesHotels())
            {
                cbHôtel.Items.Add(item.Nom);
            }
        }
    }
}
