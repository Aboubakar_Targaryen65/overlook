﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tpHotel
{
    class Chambre 
    {
        private int num;
        private int id;
        private int etage;
        private string description;

        public Chambre(int UnNum, int UnId, int UnEtage, string UneDescription)
        {
            num = UnNum;
            id = UnId;
            etage = UnEtage;
            description = UneDescription;
        }

        public int Num { get => num; set => num = value; }
        public int Id { get => id; set => id = value; }
        public int Etage { get => etage; set => etage = value; }
        public string Description { get => description; set => description = value; }
    }
}