﻿namespace tpHotel
{
    partial class frmVoirChambres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstChambres = new System.Windows.Forms.DataGridView();
            this.btnFermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).BeginInit();
            this.SuspendLayout();
            // 
            // lstChambres
            // 
            this.lstChambres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lstChambres.Location = new System.Drawing.Point(12, 12);
            this.lstChambres.Name = "lstChambres";
            this.lstChambres.Size = new System.Drawing.Size(582, 350);
            this.lstChambres.TabIndex = 4;
            // 
            // btnFermer
            // 
            this.btnFermer.Location = new System.Drawing.Point(595, 368);
            this.btnFermer.Name = "btnFermer";
            this.btnFermer.Size = new System.Drawing.Size(97, 40);
            this.btnFermer.TabIndex = 3;
            this.btnFermer.Text = "Fermer";
            this.btnFermer.UseVisualStyleBackColor = true;
            this.btnFermer.Click += new System.EventHandler(this.btnFermer_Click);
            // 
            // frmVoirChambres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 420);
            this.Controls.Add(this.lstChambres);
            this.Controls.Add(this.btnFermer);
            this.Name = "frmVoirChambres";
            this.Text = "frmVoirChambres";
            this.Load += new System.EventHandler(this.frmVoirChambres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lstChambres)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lstChambres;
        private System.Windows.Forms.Button btnFermer;
    }
}